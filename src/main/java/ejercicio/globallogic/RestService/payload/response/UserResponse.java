package ejercicio.globallogic.RestService.payload.response;

import ejercicio.globallogic.RestService.models.User;

public class UserResponse {
    
    private User user;

    private String token;

    /**
     * @param user
     * @param token
     */
    public UserResponse(User user, String token) {
        this.user = user;
        this.token = token;
    }

    

    /**
     * @return User return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return String return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

}