package ejercicio.globallogic.RestService.services;

import org.springframework.http.ResponseEntity;
import ejercicio.globallogic.RestService.payload.request.UserRequest;

public interface UserService {
    
    ResponseEntity<?> authenticate(UserRequest request);

    ResponseEntity<?> register(UserRequest request);


}